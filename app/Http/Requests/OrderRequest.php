<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
    */
    public function rules()
    {
        return [
            'products'=>'required|array',
            'products.*.product_id'=>'required|exists:products,id',
            'products.*.quantity'=>'required|gt:0',
        ];
    }

     /**
     * Get message on validation failure.
     *
     * @return exception
    */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['status' =>false, 'message' => $validator->errors()->first(),'error'=>$validator->errors()], 422));
    }
    
    /**
     * Customize message on validation failure.
     *
     * @return array
    */
    public function messages()
    {
        return [
            'products.*.quantity.gt' => 'Quantity must be greator than 0.',
            'products.*.product_id.exists' => 'Product does not exist.'             
        ];
    }
}
