<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\Ingredient;
use Mail;
use App\Mail\NotifyStock;

class OrderController extends Controller
{
     /**
     * Place new order.
     * @param OrderRequest
     * 
     * @return json
     */
    public function placeorder(OrderRequest $request) {        
        
        //Verify the availability of stock if order belongs to Burger 
        $orderStatus = [];
        foreach($request->products as $key=>$product){
            $key++;
            $productId  = $product['product_id'];
            $productQty = $product['quantity'];
            $productType = Product::find($productId)->type;
            $orderStatus[$key]['order_id'] = $key;
            $orderStatus[$key]['status'] = true;
            $orderStatus[$key]['message'] = "Order placed successfully";
            if($productType=='burger'){ //verify and update stock,only for burger
                $verifyStockArray = $this->verifyStock($productId,$productQty);
                if ( $verifyStockArray['stockAvailable'] ) {
                    $this->updateStockSendEmail($verifyStockArray['stock']);
                } else {
                    $orderStatus[$key]['status'] = false;
                    $orderStatus[$key]['message'] = $verifyStockArray['stock'];
                }
            }
            //Place order in related table
            if($orderStatus[$key]['status'] == true){
                $order = Order::create([
                    'product_id' => $productId,
                    'quantity'  => $productQty
                ]);
            }            
        }        
        return response()->json(['status'=>true,'msg'=>"order placed successfully",'data'=>$orderStatus],200);
    }
    
     /**
     * Verify ingredient stock according to provided quantity.
     * @param productId
     * @param quantity
     * 
     * @return array
     */
    public function verifyStock($productId,$quantity){
        $product = Product::find($productId);
        $stockNotAvailableMsgs = [];
        $stockAvailableFlag = 1;                
        foreach($product->ingredients as $ingredient){
            $quantityInKg = $ingredient->pivot->quantity/1000; //convert gm into kg
            $ingredientRequired = $quantityInKg*$quantity;
            if( $ingredientRequired > $ingredient->stock ){
                $stockAvailableFlag = 0;
                $stockNotAvailableMsgs[] = ucfirst($ingredient->name)." stock not available";
            } else {
                $stockNotAvailableMsgs[$ingredient->id] =  $ingredientRequired;
            }
        }
        return ['stockAvailable'=>$stockAvailableFlag,'stock'=>$stockNotAvailableMsgs];
    }

     /**
     * Update ingredient stock also send email if any stock less than 50%.
     * @param ingredientsArray
     * 
     * @return void
     */
    public function updateStockSendEmail($ingredientsArray){
        if(count($ingredientsArray)){
            foreach($ingredientsArray as $ingredientId=>$stockConsumed) {
                $updateData = [];
                //update stock
                $ingredientRow = Ingredient::find($ingredientId);
                $newStock = $ingredientRow->stock-$stockConsumed;
                $updateData['stock'] = $newStock;
                if ($newStock<$ingredientRow->email_threshhold && $ingredientRow->email_sent == 0 ){
                    $updateData['email_sent'] = 1; //This flag will reset,once Admin will update stock and stock is greater than email threshold
                    $data=['message'=>ucfirst($ingredientRow->name)." stock is running out"];
                    Mail::to(env('ADMIN_EMAIL'))->send(new NotifyStock($data));                
                }
                Ingredient::where('id',$ingredientId)->update($updateData);               
            }
        }
    }

}
