# Order Processing


## Order Flow / API

* Need to run migration along with seed ( **php artisan migrate:fresh --seed** )
* Provide MailTrap credentials in .env file to verify emails,also provide receiver email in         **ADMIN_EMAIL=youemai@xyz.com**
* Order placement
   * API Endpoint : **/api/order**
   * Sample Request: 
{
 "products":[
     {
        "product_id":1,
        "quantity":1
     },
     {
        "product_id":2,
        "quantity":10
     }
  ]
}
  * You can place multiple order in single request(according to sample json,which you provided)
  * Product_id is compulsory
  * Product_id should exist in database
  * Quantity is compulsory
  * Quantity must be greater than 0
  * If the product is of type burger then the following will verify.
    * Script will verify stock availability for each order,The order will fail if any of the      ingredients is not available according to the provided quantity.
    * Script will update stock for each ingredient being used in specific order.
    * Script will send one time email,if any ingredient is less than 50%.
    * Order will be placed.
  * If the product is of any other type than order will place without any verification.
* To verify unit test (**php artisan unit**)

## ER Diagram


![alt text](finalFoodicsProject/Foodics-ER.png)


## Postman Collection

[POSTMAN - COLLECTION - Link](https://gitlab.com/hafeez3/demo/-/blob/main/finalFoodicsProject/foodics-app.postman_collection.json)















