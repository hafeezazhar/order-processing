<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\Api\OrderController;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();        
        $this->seed();
        $this->withoutExceptionHandling();        
    }

    /**
     * A basic feature for order placement.
     *
     * @return void
     */
    public function test_place_order()
    {       
        $requestData = [
            "products" => [
                "0" => ["product_id" => 1,"quantity" => 2],
                "1" => ["product_id" => 2,"quantity" => 5],
            ]
        ];

        $response = $this->post('/api/order',$requestData);
        $response->assertStatus(200);
        
    }
   
}
