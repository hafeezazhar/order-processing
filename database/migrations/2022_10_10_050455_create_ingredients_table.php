<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment("Name of ingredient");
            $table->decimal('stock',10,2)->comment("To save the orignal stock");
            $table->decimal('email_threshhold',10,2)->comment("Email threshhold to send stock alert");
            $table->boolean('email_sent')->default(0)->comment("To controll one time email");
            $table->enum('unit',['kg','liter'])->default('kg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}
