<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ingredient;
class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ingredient::create([
            'name' => 'beef',
            'stock' => '20',
            'email_threshhold' => '10'
        ]);

        Ingredient::create([
            'name' => 'cheese',
            'stock' => '10',
            'email_threshhold' => '5'
        ]);

        Ingredient::create([
            'name' => 'onion',
            'stock' => '1',
            'email_threshhold' => '0.5'
        ]);
    }
}
